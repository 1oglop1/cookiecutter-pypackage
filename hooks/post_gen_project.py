#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Standalone script being triggered after the template rendered."""

import os
from subprocess import call

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)


def remove_file(filepath):
    """Wipe the given file out of the file system."""
    os.remove(os.path.join(PROJECT_DIRECTORY, filepath))


if __name__ == '__main__':
    if '{{ cookiecutter.create_author_file }}' != 'y':
        remove_file('AUTHORS.rst')
        remove_file('docs/_source/authors.rst')

    if 'Not open _source' == '{{ cookiecutter.open_source_license }}':
        remove_file('LICENSE')

    call(['git', 'init'], cwd=PROJECT_DIRECTORY)  # Initialize git repo, needed for setuptools_scm
