#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: {{ cookiecutter.project_slug }}.py
"""
Main module file.

Put your classes here.
"""  # pragma:

import logging

logger = logging.getLogger(__name__)


class {{ (cookiecutter.project_slug.title()).replace('_', '') }}:  # pylint: disable=too-few-public-methods
    """Main class {{ (cookiecutter.project_slug.title()).replace('_', '') }}."""

    def __init__(self):
        """Initialize of the {{ (cookiecutter.project_slug.title()).replace('_', '') }} class."""
        # instance logger
        self.logger = logging.getLogger('{base}.{suffix}'.format(
            base=logger.name, suffix=self.__class__.__name__))

    @staticmethod
    def simple_add(num_a, num_b):
        """
        Produce a sum of two numbers.

        Parameters
        ----------
        num_a : number
            Number A.
        num_b : number
            Number B.

        Returns
        -------
        number
            Sum of two numbers.

        """
        return num_a + num_b

    def method(self):  # pragma: no cover
        """Sample method to show log."""
        print("I'm method!")
        self.logger.info('I log from {{ cookiecutter.project_slug }}')
