.. {{ cookiecutter.project_slug }} documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to {{ cookiecutter.project_name }}'s documentation!
{{ '='*(cookiecutter.project_name | length  + 28) }}

Contents:

.. toctree::
   :maxdepth: 2
   :glob:

   readme
   installation
   usage
   contributing
   {% if cookiecutter.create_author_file == 'y' -%}
   authors
   {% endif -%}
   human_docs/*
   modules
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

