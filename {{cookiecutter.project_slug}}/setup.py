#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Distribution package description for {{ cookiecutter.project_name }}.

Check `setup.cfg` for the project metadata
"""

__requires__ = ('setuptools>=36.3.0',)
"""rwt-compliant requirement list, means nothing unless invoked via `rwt` tool.

Ref: https://pypi.python.org/pypi/rwt#replacing-setup-requires
"""
import sys  # noqa: E402
from setuptools import setup  # noqa: E402
from setuptools.config import read_configuration  # noqa: E402


needs_pytest = {'test', 'pytest'}.intersection(sys.argv)
pytest_runner = ['pytest-runner'] if needs_pytest else []

declarative_setup_params = read_configuration('setup.cfg')

extras = declarative_setup_params['options'].get('extras_require', {})
extras['testing'] = declarative_setup_params['options']['tests_require']

setup_params = {

    **declarative_setup_params['metadata'],
    **declarative_setup_params['options'],
    # 'extras_require':{
    #     **declarative_setup_params['options']['extras_require'],
    #     'testing': declarative_setup_params['options']['tests_require']
    # },
    # waiting for fix: https://github.com/pypa/setuptools/pull/1150
    'setup_requires': [
        'setuptools-scm>=1.15.0',
        'setuptools-git',

    ] + pytest_runner,
    # Configure setuptools_scm for CVS-based semantic versioning support
    # cannot be used in setup.cfg, see: https://github.com/pypa/setuptools_scm/issues/181
    'use_scm_version': True,
}


__name__ == '__main__' and setup(**setup_params)
