#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Test suite for cookiecutter template rendering."""

from contextlib import contextmanager
import datetime
import os
import shlex
import subprocess

from cookiecutter.utils import rmtree
import pytest


@contextmanager
def bake_in_temp_dir(cookies, *args, **kwargs):
    """Delete the temporary directory that is created when executing the tests.

    :param cookies: pytest_cookies.Cookies, cookie to be baked and its temporal files will be removed
    """
    result = cookies.bake(*args, **kwargs)
    try:
        yield result
    finally:
        rmtree(str(result.project))


def run_inside_dir(command, dirpath):
    """Run a command from inside a given directory, returning the exit status.

    :param command: Command that will be executed
    :param dirpath: String, path of the directory the command is being run.
    """
    return subprocess.check_call(shlex.split(command), cwd=dirpath)


def run_in_sh_inside_dir(command, dirpath):
    """Run a command from inside a given directory, returning the exit status.

    :param command: Command that will be executed
    :param dirpath: String, path of the directory the command is being run.
    """
    return subprocess.check_call(['sh', '-c', command], cwd=dirpath)


def check_output_inside_dir(command, dirpath):
    """Run a command from inside a given directory, returning the command output."""
    return subprocess.check_output(shlex.split(command), cwd=dirpath, stderr=subprocess.STDOUT)


def test_year_compute_in_license_file(cookies):
    """Check that correct year is included within license file."""
    with bake_in_temp_dir(cookies) as result:
        license_file_path = result.project.join('LICENSE')
        now = datetime.datetime.now()
        assert str(now.year) in license_file_path.read()


def project_info(result):
    """Get toplevel dir, project_slug, and project dir from baked cookies."""
    project_path = str(result.project)
    project_slug = os.path.split(project_path)[-1]
    project_dir = os.path.join(project_path, project_slug)
    return project_path, project_slug, project_dir


def test_bake_with_defaults(cookies):
    """Make sure that the template is rendered correctly with default options."""
    with bake_in_temp_dir(cookies) as result:
        assert result.project.isdir()
        assert result.exit_code == 0
        assert result.exception is None

        found_toplevel_files = [f.basename for f in result.project.listdir()]
        assert 'setup.py' in found_toplevel_files
        assert 'tests' in found_toplevel_files
        # assert 'tox.ini' in found_toplevel_files  # commented out due to possibility of reintroduction


@pytest.mark.parametrize(
    'full_name', [
        None,
        'name "quote" name',
        "O'connor",
    ]
)
def test_bake_and_run_tests(cookies, full_name):
    """Ensure that a `full_name` with double quotes, apostrophes or default one does not break setup.py."""
    kwargs = {'extra_context': {'full_name': full_name}} if full_name else {}
    with bake_in_temp_dir(cookies, **kwargs) as result:
        assert result.project.isdir()
        run_in_sh_inside_dir(
            'python .gitlab/install_prerequisites.py',
            str(result.project)
        )
        assert run_inside_dir('python setup.py test', str(result.project)) == 0


def test_bake_without_author_file(cookies):
    """Make sure that authors list is not included if turned off."""
    with bake_in_temp_dir(cookies, extra_context={'create_author_file': 'n'}) as result:
        found_toplevel_files = [f.basename for f in result.project.listdir()]
        assert 'AUTHORS.rst' not in found_toplevel_files
        doc_files = [f.basename for f in result.project.join('docs').listdir()]
        assert 'authors.rst' not in doc_files

        # Assert there are no spaces in the toc tree
        docs_index_path = result.project.join('docs/_source/index.rst')
        with open(str(docs_index_path)) as index_file:
            assert 'contributing\n   human_docs' in index_file.read()


def test_make_help(cookies):
    """Make sure that Makefile is generated."""
    with bake_in_temp_dir(cookies) as result:
        output = check_output_inside_dir('make help', str(result.project))
        assert b'WARNING, all commands issued here can be affected by your VENV! use pip install -e .' in output


@pytest.mark.parametrize(
    'license, target_string', [
        ('MIT license', 'MIT '),
        ('BSD license', 'Redistributions of source code must retain the above copyright notice, this'),
        ('ISC license', 'ISC License'),
        ('Apache Software License 2.0', 'Licensed under the Apache License, Version 2.0'),
        ('GNU General Public License v3', 'GNU GENERAL PUBLIC LICENSE'),
    ]
)
def test_bake_selecting_license(cookies, license, target_string):
    """Check that the correct open source license is included."""
    with bake_in_temp_dir(cookies, extra_context={'open_source_license': license}) as result:
        assert target_string in result.project.join('LICENSE').read()
        assert license in result.project.join('setup.cfg').read()


def test_bake_not_open_source(cookies):
    """Check that the correct license text is included if it's closed source."""
    with bake_in_temp_dir(cookies, extra_context={'open_source_license': 'Not open source'}) as result:
        found_toplevel_files = [f.basename for f in result.project.listdir()]
        assert 'setup.py' in found_toplevel_files
        assert 'For licensing, please contact the author at' in result.project.join('LICENSE').read()
        assert 'OSI Approved' not in result.project.join('setup.cfg').read()


def test_using_pytest(cookies):
    """Check that pytest integration works seamlessly.

    Also ensures that:
    * example test module is generated
    * pytest in integrated within distribution
    """
    with bake_in_temp_dir(cookies) as result:
        assert result.project.isdir()
        project_path, project_slug, project_dir = project_info(result)
        test_file_path = result.project.join(f'tests/conftest.py')
        lines = test_file_path.readlines()
        assert 'import pytest' in ''.join(lines)
        run_in_sh_inside_dir(
            'python .gitlab/install_prerequisites.py',
            str(result.project)
        )
        # Test the test alias (which invokes pytest)
        assert run_inside_dir('python setup.py test', str(result.project)) == 0


@pytest.mark.precommit
def test_pre_commit_compliance(cookies):
    """Check that pre-commit tool confirms no convention errors."""
    with bake_in_temp_dir(cookies) as result:
        assert result.project.isdir()
        assert run_inside_dir('pip install pre-commit', str(result.project)) == 0
        assert run_inside_dir('git add -A', str(result.project)) == 0  # Stage files, so that pre-commit picks them up
        assert run_inside_dir('pre-commit run --all-files', str(result.project)) == 0


def test_logging(cookies):
    """Check if logging works."""
    with bake_in_temp_dir(cookies) as result:
        project_name = os.path.basename(str(result.project))
        assert run_inside_dir('pip install -e .', str(result.project)) == 0
        output = check_output_inside_dir(project_name, str(result.project))
        assert f'I log from {project_name}'.encode() in output


def test_version(cookies):
    """Check if setuptools_scm works correctly."""
    with bake_in_temp_dir(cookies) as result:
        project_name = os.path.basename(str(result.project))
        run_inside_dir('pip install -e .', str(result.project))
        output = check_output_inside_dir(f'python -c "import {project_name}; print({project_name}.__version__)"',
                                         str(result.project))
        assert b'0.1.dev0' in output
